#primero importamos gi y json
import gi
import json
gi.require_version("Gtk","3.0")
from gi.repository import Gtk

#creamos una funcion que habra nuestro archivo json
def open_file():

    try:
        with open("pacientes.json", 'r') as archivo:
            data = json.load(archivo)
    except IOError:
        data = []
    return data


#creamos un funcion que guarde los archivos editados y agregados 
def save_file(data):
    with open("pacientes.json", "w") as arcchivo:
        json.dump(data, arcchivo , indent=4) 

#creamos una clase para la ventana principal del programa 
class Ventana_principal():

    def __init__(self):

        # Ventana Principal 
        self.builder = Gtk.Builder()
        self.builder.add_from_file("proyecto_1.ui")
        ventana = self.builder.get_object("ventana_principal")
        #le damos un titulo a la ventana
        ventana.set_title("- - - Bienvenidos a Veterinaria Dr Pol - - -")
        ventana.connect("destroy", Gtk.main_quit)
        #le damos una resolucion
        ventana.set_default_size(500,400)
        ventana.show_all()
        
        # Entrada de Contraseña Veterinario
        self.entrada_vet = self.builder.get_object("entrada_contraseña_veterinari")
        #el entry funcionara al ingresar la contraseña y apretar enter el cual habre la ventana del
        #administrador
        self.entrada_vet.connect("activate",self.enter_contraseña)
        #llamamos al boton paciente el cual abre la ventana para el paciente
        boton_paciente = self.builder.get_object("boton_paciente")
        boton_paciente.connect("clicked",self.clicked_boton_adelante)
        #llamamos al boton informacion y hacemos que funcione con un click para habrir la ventana de
        #informacion
        boton_info = self.builder.get_object("info_btn")
        boton_info.connect("clicked",self.clicked_info)
    #creamos la funcion para que funcione la contraseña 
    def enter_contraseña(self,enter = None):
        #creamos una contraseña
        contrasenia = "VtrPol_2k2k"
        #creamos un if para identificar si la contraseña fue escrita 
        #correctamente 
        if contrasenia == self.entrada_vet.get_text():
            self.entrada_vet.set_text("")
            dialogo = Ventana_Dialogo_Veterin()
        else:
            Close_dialogo = Advertencia_Ventana()
            self.entrada_vet.set_text("")
    #se crea la funcion para que se habra la ventana informacion        
    def clicked_info(self,btn = None):
        Informacion = Info_dialogo()
    #se crea la funcion para habrir la ventana del paciente
    def clicked_boton_adelante(self, btn = None):
        Dialogo_Sala = Ventana_Paciente()


#se crea la clase para la ventana principal del administrador 
class Ventana_Dialogo_Veterin():
    #se crea la funcion principal
    def __init__(self):
        
        #se llaman al protecto glade 
        builder = Gtk.Builder()
        builder.add_from_file("proyecto_1.ui")
        #se llaman a las ventana del administrador
        self.dialogo_veter = builder.get_object("ventana_dialogo_1")
        self.dialogo_veter.set_title("- - Reservaciones de Veterinaria Dr Pol - -")
        #se ajusta la resolucion de la ventana 
        self.dialogo_veter.set_default_size(800,600)
        self.dialogo_veter.show_all()

        #se llaman a los botontes que tiene la ventana  y se hace que cada uno realice sus respectivas 
        #actividades
        boton_agregar = builder.get_object("agregar_btn")
        boton_agregar.connect("clicked",self.clicked_agregar)
        boton_editar = builder.get_object("editar_btn")
        boton_editar.connect("clicked", self.clicked_editar)
        boton_eliminar = builder.get_object("eliminar_btn")
        boton_eliminar.connect("clicked", self.delete_selected_data)
        boton_ayudar = builder.get_object("btn_ayudar")
        boton_ayudar.connect("clicked",self.clicked_ayuda)
        #se llama al treeview y se arregla el tamaño del treeview
        self.tree = builder.get_object("treePacientes")
        self.modelo = Gtk.ListStore(str,str,str,str,str,str,str)
        self.tree.set_model(model = self.modelo)

        #se les da los nombre a cada columna 
        nombre_columnas = ("Dueño","Rut","Mascota","Id Mascota","Fecha","Hora","Nombre de Mascota")
        cell = Gtk.CellRendererText()

        #se crea un for para rrecorre el treeview 
        for item in range(len(nombre_columnas)):
            columna = Gtk.TreeViewColumn(nombre_columnas[item],cell,text = item)
            self.tree.append_column(columna)

        self.load_data_from_json()

    #se crea una funcion que cargue el json
    def load_data_from_json(self):
        
        #se recorre el json
        datos = open_file()
        for item in datos:
            line = [x for x in item.values()]
            self.modelo.append(line)

    #una funcion que elimina todo el json
        def delete_all_data(self):
        for index in range(len(self.modelo)):
            iter_ = self.modelo.get_iter(0)
            self.modelo.remove(iter_)

    #una funcion que elimine la fila seleccionada
    def delete_selected_data(self , btn = None):
        model,it =self.tree.get_selection().get_selected()
        if model is None or it is None:
            return
        data = open_file()
        for item in data:
            #se pone el nombre de la columna principal para saber que fila fue seleccionada
            if item["Dueño"] == model.get_value(it,0):
                data.remove(item)
        save_file(data)
        self.delete_all_data()
        self.load_data_from_json()


#se crea la funcion para el boton agregar 
    def clicked_agregar(self,btn = None):

        Vent_agregar = Ventana_Dialogo_Process()
        response = Vent_agregar.dialogo_2.run()

        #se borra el json y se vuelve a cargar con los nuevos datos
        if response == Gtk.ResponseType.OK:
            self.delete_all_data()
            self.load_data_from_json()
            pass
        elif response == Gtk.ResponseType.CANCEL:
            pass
        Vent_agregar.dialogo_2.destroy()


    #se crea la funcion para editar la fila seleccionada
    def clicked_editar(self ,btn =None):

        #se identifican los nombres de cada columna para poder editar su contenido
        model,it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return
        ventana_dialogo = Ventana_Dialogo_Process()
        ventana_dialogo.nombre.set_text(model.get_value(it,0))
        ventana_dialogo.rut.set_text(model.get_value(it,1))
        ventana_dialogo.mascota.set_text(model.get_value(it,2))
        ventana_dialogo.id_masc.set_text(model.get_value(it,3))
        ventana_dialogo.hora.append_text(model.get_value(it,5))
        ventana_dialogo.nomb_masc.set_text(model.get_value(it,6))

        response = ventana_dialogo.dialogo_2.run()

        #se pone el nombre de la columna principal para saber que fila fue seleccionada
        if response == Gtk.ResponseType.OK:
            data = open_file()
            for item in data:
                if item["Dueño"] == model.get_value(it,0):
                    data.remove(item)
            save_file(data)
            self.delete_all_data()
            self.load_data_from_json()
        elif response == Gtk.ResponseType.CANCEL:
            pass
        ventana_dialogo.dialogo_2.destroy()

    #se crea la funcion para llamar a la ventana ayuda
    def clicked_ayuda(self, btn = None):
        Ventana_ayuda = Ayuda_ventana()

#se crea la clase para la ventana  de procesos en el cual
#se agregan o de editan los datos
class Ventana_Dialogo_Process():
    def __init__(self):

#se llaman a los respectivos archivos como en las otras clases
        builder = Gtk.Builder()
        builder.add_from_file("proyecto_1.ui")
        self.dialogo_2 = builder.get_object("ventana_dialogo_2")
        self.dialogo_2.set_title("* - - - Citación - - - *")
        self.dialogo_2.set_default_size(800,600)
#se llama al boton aceptar y que realice una accion al hacer click
        boton_ok = builder.get_object("aceptar_btn")
        boton_ok.connect("clicked" , self.boton_ok_clicked)
#se llama el boton cancelar 
        boton_cancelar = builder.get_object("cancelar_btn")
        #se llaman los respectivos entry y se les pone nombre
        self.nombre = builder.get_object("entrada_dueño")
        #self.nombre.connect("acivate", self.entrada_name_en)
        self.rut = builder.get_object("entrada_rut")
        #self.rut.connect("activate", self.entrada_rut_en)
        self.mascota = builder.get_object("entrada_mascota")
        #self.mascota.connect("activate", self.entrada_pet_en)
        self.id_masc = builder.get_object("entrada_id_masc")
        #self.id_masc.connect("activate", self.entrada_id_pet_en)
        self.nomb_masc = builder.get_object("entrada_nomb_masc")
        #self.nomb_masc.connect("activate", self.entrada_name_pet_en)
        
        #se crean los combobox
        #combo BOXs
        lista = ["--Seleccione--"]
        lista1 = ["--Seleccione--"]
        lista2 = ["--Seleccione--","2021","2022"]
        lista3 = ["--Seleccione--"]
        #Dias
        for dias in range(30):
            if dias < 9:
                diasstr = f'0{dias+1}'
            else:
                diasstr = f'{dias+1}'
            lista.append(diasstr)
        #Meses
        for meses in range(12):
            if meses < 9:
                mesesstr = f'0{meses+1}'
            else:
                mesesstr = f'{meses+1}'
            lista1.append(mesesstr)  
        #Horas
        for horas in range(9):
            horasstr = f'{horas+8}:00'
            lista3.append(horasstr)

#se crean los combobox de:
#dia
        self.dia = builder.get_object("comboboxtext_dia")
        for item in lista:
            self.dia.append_text(item)
        self.dia.set_active(0)
        self.dia.connect("changed", self.on_combo_changed_1)
        #mes
        self.mes = builder.get_object("comboboxtext_mes")
        for item1 in lista1:
            self.mes.append_text(item1) 
        self.mes.set_active(0)
        self.mes.connect("changed", self.on_combo_changed_2)
       #año
        self.anio = builder.get_object("comboboxtext_anio")
        for item2 in lista2:
            self.anio.append_text(item2)
        self.anio.set_active(0)
        self.anio.connect("changed", self.on_combo_changed_3)
        #hora
        self.hora = builder.get_object("comboboxtext_hora")
        for item3 in lista3:
            self.hora.append_text(item3)
        self.hora.set_active(0)
        self.hora.connect("changed", self.on_combo_changed_4)
        
#se muestra la ventana
        self.dialogo_2.show_all()        
    #se crean las funciones para los combos box y que funcione la opcion seleccionada
    def on_combo_changed_1(self,cmb = None):
        
        if self.dia.get_active_text() == "--Seleccione--":
            return
        else:
            self.valor_1 = self.dia.get_active_text()

#se crean las funciones para los combos box y que funcione la opcion seleccionada
    def on_combo_changed_2(self,cmb = None):
        if self.mes.get_active_text() == "--Seleccione--":
            return self.valor_2 
        else:
            self.valor_2 = self.mes.get_active_text()
#se crean las funciones para los combos box y que funcione la opcion seleccionada
    def on_combo_changed_3(self, cmb = None):
        if self.anio.get_active_text() == "--Seleccione--":
            return 
        else:
            self.valor_3 = self.anio.get_active_text()
            self.temp3 = self.valor_3
#se crean las funciones para los combos box y que funcione la opcion seleccionada
    def on_combo_changed_4(self, cmb = None):
        if self.hora.get_active_text() == "--Seleccione--":
            return self.valor_4
        else:
            self.valor_4 = self.hora.get_active_text()
#se crea la funcion agregar los datos editados en la fila seleccionada
    def boton_ok_clicked(self, btn = None):
        name_text = self.nombre.get_text()
        rut_text = self.rut.get_text()
        masc_text = self.mascota.get_text()
        id_masc_text = self.id_masc.get_text()
        id_masc_text = f'#{id_masc_text}'
        nomb_masc_text = self.nomb_masc.get_text()
        self.fecha_text = f'{self.valor_1}-{self.valor_2}-{self.valor_3}'
        hora_text = self.valor_4

        data = open_file()
        new_data = {"Dueño": name_text,
                    "Rut" : rut_text,
                    "Mascota" : masc_text,
                    "Id_Mascota" : id_masc_text,
                    "Fecha" : self.fecha_text,
                    "Hora": hora_text,
                    "Nombre_de_Mascota": nomb_masc_text
                    }
        data.append(new_data)
        save_file(data)

#se crea la clase para la ventana de informacion
class Info_dialogo():

    def __init__(self):
#se llama se llama a los archivos necesarios 
        builder = Gtk.Builder()
        builder.add_from_file("proyecto_1.ui")
        self.info_ventana = builder.get_object("about_informacion")
        self.info_ventana.show_all()

#se crea la ventana para la ventana de advertencia 
class Advertencia_Ventana():
    def __init__(self):
#se llama se llama a los archivos necesarios 
        builder = Gtk.Builder()
        builder.add_from_file("proyecto_1.ui")
        self.close_ventana = builder.get_object("Error_dialogo")
        boton_cerrar = builder.get_object("btn_close")
        boton_cerrar.connect("clicked",self.clicked_close)
        self.close_ventana.show_all()
#se crea la funcion para cerrar la ventana 
    def clicked_close(self, btn = None):
        self.close_ventana.destroy()

#se crea la clase para la ventana del paciente
class Ventana_Paciente():
    def __init__(self):
#se llama se llama a los archivos necesarios 
        builder = Gtk.Builder()
        builder.add_from_file("proyecto_1.ui")
        self.dialogo_veter = builder.get_object("ventana_dialogo_4")
        self.dialogo_veter.set_title("- - Reservaciones de Veterinaria Dr Pol - -")
        self.dialogo_veter.set_default_size(800,600)
        self.dialogo_veter.show_all()

#se llama al boton agregar 2 y que realice su respectiva funcion al hacer click
        boton_agregar = builder.get_object("agregar_btn2")
        boton_agregar.connect("clicked",self.clicked_agregar2)
#se llama al treeview y se arregla el tamaño del treeview
        self.tree = builder.get_object("treePacientes1")
        self.modelo = Gtk.ListStore(str,str,str,str,str,str,str)
        self.tree.set_model(model = self.modelo)
#se les da los nombre a cada columna 
        nombre_columnas = ("Dueño","Rut","Mascota","Id Mascota","Fecha","Hora","Nombre de Mascota")
        cell = Gtk.CellRendererText()
        #se crea un for para rrecorre el treeview
        for item in range(len(nombre_columnas)):
            columna = Gtk.TreeViewColumn(nombre_columnas[item],cell,text = item)
            self.tree.append_column(columna)

        self.load_data_from_json()

#se crea una funcion que cargue el json
    def load_data_from_json(self):
#se recorre el json
        datos = open_file()
        for item in datos:
            line = [x for x in item.values()]
            self.modelo.append(line)

#una funcion que elimina todo el json
    def delete_all_data(self):
        for index in range(len(self.modelo)):
            iter_ = self.modelo.get_iter(0)
            self.modelo.remove(iter_)




#se crea la funcion para el boton agregar
    def clicked_agregar2(self,btn = None):

        Vent_agregar2 = Ventana_Dialogo_Process()
        response = Vent_agregar2.dialogo_2.run()
#se elimina el json y se vuelve a crear con el dato editado 
        if response == Gtk.ResponseType.OK:
            self.delete_all_data()
            self.load_data_from_json()
            pass
        elif response == Gtk.ResponseType.CANCEL:
            pass
        Vent_agregar2.dialogo_2.destroy()
#se crea una clase para la ventana ayuda
class Ayuda_ventana():

    def __init__(self):
#se llaman a los archivos necesarios 
        builder = Gtk.Builder()
        builder.add_from_file("proyecto_1.ui")
        self.ayuda_ventanita = builder.get_object("about_ayuda")
        self.ayuda_ventanita.show_all()




if __name__ == "__main__":
    Ventana_principal()
    Gtk.main()


